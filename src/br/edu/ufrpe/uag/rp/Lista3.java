/**
 * 
 */
package br.edu.ufrpe.uag.rp;

import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import br.edu.ufrpe.uag.rp.util.ImagemDigital;

/**
 * @author israel
 *
 */
public class Lista3 {

	/**
	 * 
	 */
	public Lista3() {
		// TODO Auto-generated constructor stub
	}

	public void questaoOitoListaDois() {
		int[][] imagem1 = ImagemDigital.carregarImagem("Fig0320(1)(top_left).png");
		int[] histograma1 = calculaHistograma(imagem1);
		int[] histograma1Norme = equalizaHistograma(histograma1);
		salveToCSV(histograma1, "top_left");
		salveToCSV(histograma1Norme, "top_left_norm");
		int[][] equalizada1 = equalizaImage(imagem1, map);
		ImagemDigital.plotarImagem(equalizada1, "equalizada");

		int[][] imagem2 = ImagemDigital.carregarImagem("Fig0320(2)(2nd_from_top).png");
		int[] histograma2 = calculaHistograma(imagem2);
		int[] histograma2Norme = equalizaHistograma(histograma2);
		salveToCSV(histograma2, "2nd_from_top");
		salveToCSV(histograma2Norme, "2nd_from_top_norm");
		int[][] equalizada2 = equalizaImage(imagem2, map);
		ImagemDigital.plotarImagem(equalizada2, "equalizada");

		int[][] imagem3 = ImagemDigital.carregarImagem("Fig0320(3)(third_from_top).png");
		int[] histograma3 = calculaHistograma(imagem3);
		int[] histograma3Norme = equalizaHistograma(histograma3);
		salveToCSV(histograma3, "third_from_top");
		salveToCSV(histograma3Norme, "third_from_top_norm");
		int[][] equalizada3 = equalizaImage(imagem3, map);
		ImagemDigital.plotarImagem(equalizada3, "equalizada");

		int[][] imagem4 = ImagemDigital.carregarImagem("Fig0320(4)(bottom_left).png");
		int[] histograma4 = calculaHistograma(imagem4);
		int[] histograma4Norme = equalizaHistograma(histograma4);
		salveToCSV(histograma4, "bottom_left");
		salveToCSV(histograma4Norme, "bottom_left_norm");
		int[][] equalizada4 = equalizaImage(imagem4, map);
		ImagemDigital.plotarImagem(equalizada4, "equalizada");

		int[][] imagem5 = ImagemDigital.carregarImagem("Fig0323(a)(mars_moon_phobos).png");
		int[] histograma5 = calculaHistograma(imagem5);
		int[] histograma5Norme = equalizaHistograma(histograma5);
		salveToCSV(histograma5, "mars_moon_phobos");
		salveToCSV(histograma5Norme, "mars_moon_phobos_norm");
		int[][] equalizada5 = equalizaImage(imagem5, map);
		ImagemDigital.plotarImagem(equalizada5, "equalizada");
	}

	private int[][] equalizaImagem(int[][] imagem, int[] histograma) {
		int[][] retorno = new int[imagem.length][imagem[0].length];
		for (int i = 0; i < imagem.length; i++) {
			for (int j = 0; j < imagem[0].length; j++) {

			}
		}
		return retorno;
	}

	// mapeamento no do novo histograma
	int[] map;

	private int[] equalizaHistograma(int[] histograma) {

		map = new int[histograma.length];

		int[] newHistograma = new int[histograma.length];

		double[] percentagens = new double[histograma.length];

		double[] somaProbabilidades = new double[histograma.length];

		int soma = 0;
		for (Integer i : histograma) {
			soma += i;
		}

		for (int i = 0; i < histograma.length; i++) {
			percentagens[i] = (double) histograma[i] / soma;
			for (int j = 0; j <= i; j++) {
				somaProbabilidades[i] += percentagens[j];
			}

			somaProbabilidades[i] = (double) somaProbabilidades[i] * (255);

			newHistograma[i] += histograma[(int) somaProbabilidades[i]];

			map[i] = (int) somaProbabilidades[i];

		}
		return newHistograma;
	}

	private int[][] equalizaImage(int[][] imagem, int[] map) {
		int[][] retorno = new int[imagem.length][imagem[0].length];
		for (int i = 0; i < imagem.length; i++) {
			for (int j = 0; j < imagem[0].length; j++) {
				retorno[i][j] = map[imagem[i][j]];
			}
		}
		return retorno;
	}

	private void salveToCSV(int[][] imagem, String nome) {
		int[] histograma = calculaHistograma(imagem);
		List<Integer> list = new ArrayList<>();
		for (Integer i : histograma) {
			list.add(i);
		}
		writeToCSV(list, nome);
	}

	private void salveToCSV(int[] histograma, String nome) {
		List<Integer> list = new ArrayList<>();
		for (Integer i : histograma) {
			list.add(i);
		}
		writeToCSV(list, nome);
	}

	public void questaoUm() {
		int[][] imagem = ImagemDigital.carregarImagem("Fig0333(a)(test_pattern_blurring_orig).png");

		ImagemDigital.plotarImagem(aplicaMedia(imagem, 3, 3), "3x3.png");
		ImagemDigital.plotarImagem(aplicaMedia(imagem, 5, 5), "5x5.png");
		ImagemDigital.plotarImagem(aplicaMedia(imagem, 9, 9), "9x9.png");
		ImagemDigital.plotarImagem(aplicaMedia(imagem, 15, 15), "15x15.png");
		ImagemDigital.plotarImagem(aplicaMedia(imagem, 35, 35), "35x35.png");
		ImagemDigital.plotarImagem(aplicaMedia(imagem, 23, 23), "23x23.png");
		ImagemDigital.plotarImagem(aplicaMedia(imagem, 25, 25), "25x25.png");
		ImagemDigital.plotarImagem(aplicaMedia(imagem, 45, 45), "45x45.png");
	}

	private int[][] aplicaMedia(int[][] imagem, int janela_w, int janela_h) {
		int[][] retorno = new int[imagem.length][imagem[0].length];

		// percorre cada pixel
		for (int i = janela_w / 2; i < imagem.length - janela_w / 2; i++) {
			for (int j = janela_h / 2; j < imagem[0].length - janela_h / 2; j++) {
				int valor = 0;
				// percorre a janela da media
				for (int w = i - (janela_w / 2); w <= i + (janela_w / 2); w++) {
					for (int h = j - (janela_h / 2); h <= j + (janela_h / 2); h++) {
						valor += imagem[w][h];
					}
				}
				retorno[i][j] = valor / (janela_h * janela_w);
			}
		}

		return retorno;
	}

	public void questaoDois() {
		int[][] imagem = ImagemDigital.carregarImagem("Fig0334(a)(hubble-original).png");
		ImagemDigital.plotarImagem(imagem, "questaoDois");

		ImagemDigital.plotarImagem(aplicaMedia(imagem, 15, 15), "questaoDois");

		int[][] img2 = limiar(aplicaMedia(imagem, 15, 15), 64);

		for (int i = 0; i < img2.length; i++) {
			for (int j = 0; j < img2[0].length; j++) {
				img2[i][j] = imagem[i][j] * img2[i][j] / 255;
			}
		}
		ImagemDigital.plotarImagem(img2, "questaoDois");
		ImagemDigital.plotarImagem(limiar(aplicaMedia(imagem, 15, 15), 64), "questaoDois");
	}

	public void questaoTres() {
		int[][] imagem = ImagemDigital.carregarImagem("Fig0335(a)(ckt_board_saltpep_prob_pt05).png");

		ImagemDigital.plotarImagem(aplicaMedia(imagem, 3, 3), "MÉDIA 3x3");
		ImagemDigital.plotarImagem(aplicaMediana(imagem, 3, 3), "MEDIANA 3x3");
	}

	public void questaoQuatro() {
		int[][] imagem = ImagemDigital.carregarImagem("Fig0338(a)(blurry_moon).png");
		int[][] filtro = { { 0, -1, 0 }, { 0, 4, 0 }, { 0, -1, 0 } };
		ImagemDigital.plotarImagem(soma(aplicarLaplaciano(imagem, filtro), imagem, 1), "Laplaciano 1");

		int[][] imagem2 = ImagemDigital.carregarImagem("Fig0338(a)(blurry_moon).png");
		int[][] filtro2 = { { -1, -1, -1 }, { -1, 8, -1 }, { -1, -1, -1 } };
		ImagemDigital.plotarImagem(soma(aplicarLaplaciano(imagem2, filtro2), imagem, 1), "Laplaciano 2");
	}

	public void questaoCinco() {
		int[][] imagem = ImagemDigital.carregarImagem("Fig0340(a)(dipxe_text).png");

		int[][] borrada = aplicaMedia(imagem, 3, 3);
		int[][] mascara = subtrai(imagem, borrada);

		ImagemDigital.plotarImagem(borrada, "resultado");
		ImagemDigital.plotarImagem(imagem, "resultado");

		ImagemDigital.plotarImagem(soma(imagem, mascara, 8), "resultado");
		ImagemDigital.plotarImagem(soma(imagem, mascara, 4), "resultado");
		ImagemDigital.plotarImagem(soma(imagem, mascara, 6), "resultado");
	}

	public void questaoSeis() {
		int[][] imagem = ImagemDigital.carregarImagem("Fig0340(a)(dipxe_text).png");

		ImagemDigital.plotarImagem(imagem, "ORIGINAL");

		int[][] sobel = aplicaFiltro(imagem, new int[][] { { -1, -2, -1 }, { 0, 0, 0 }, { 1, 2, 1 } });
		ImagemDigital.plotarImagem(soma(sobel, imagem, 1), "Sobel");

		int[][] laplaciano = aplicaFiltro(imagem, new int[][] { { 0, -1, 0 }, { -1, 4, -1 }, { 0, -1, 0 } });
		ImagemDigital.plotarImagem(soma(laplaciano, imagem, 1), "Laplaciano");
	}

	public void questaoSete() {
		int[][] imagem = ImagemDigital.carregarImagem("Fig1007(a)(wirebond_mask).png");
		// imagem = aplicaMedia(imagem, 3, 3);

		int[][] imagemGX = ressaltaHorizontal(imagem);
		int[][] imagemGY = ressaltaVertical(imagem);

		// int[][] imagem45 = ressaltaAngulo(imagem, 45);
		// int[][] imagemM45 = ressaltaAngulo(imagem, -45);
		int[][] imagem45 = aplicaFiltro(imagem, new int[][] { { 0, 1, 1 }, { -1, 0, 1 }, { -1, -1, 0 } });
		int[][] imagemM45 = aplicaFiltro(imagem, new int[][] { { -1, -1, 0 }, { -1, 0, 1 }, { 0, 1, 1 } });

		ImagemDigital.plotarImagem(imagemGX, "Horizontal");
		ImagemDigital.plotarImagem(imagemGY, "Vertical");

		ImagemDigital.plotarImagem(imagem45, "45º");
		ImagemDigital.plotarImagem(imagemM45, "135º");
	}

	public void questaoOito() {
		int[][] imagem = ImagemDigital.carregarImagem("Fig1026(a)(headCT-Vandy).png");
		List<Integer> list = new LinkedList<>();
		for (int i = 0; i < imagem.length; i++) {
			for (int j = 0; j < imagem[0].length; j++) {
				list.add(imagem[i][j]);
			}
		}
		writeToCSV(list, "questaoOito");
	}

	public void questaoNove() {
		int[][] imagem = ImagemDigital.carregarImagem("Fig1016(a)(building_original).png");

		int[][] magnitude = magnitude(imagem);
		int[][] magnitudeMedia = magnitude(aplicaMedia(imagem, 5, 5));
		int[][] magnitudeLimiar = limiar(magnitude, 80);
		int[][] magnitudeMediaLimiar = limiar(magnitudeMedia, 80);

		Map<String, List<Integer>> map = new HashMap<>();
		List<Map<String, List<Integer>>> list = new ArrayList<>();

		map.put("magnitude", linha(magnitude, 100));
		list.add(map);

		map = new HashMap<>();
		map.put("magnitudeMedia", linha(magnitudeMedia, 100));
		list.add(map);

		map = new HashMap<>();
		map.put("magnitudeLimiar", linha(magnitudeLimiar, 100));
		list.add(map);

		map = new HashMap<>();
		map.put("magnitudeMediaLimiar", linha(magnitudeMediaLimiar, 100));
		list.add(map);

		ImagemDigital.plotarImagem(magnitude, "MAGNITUDE");
		ImagemDigital.plotarImagem(magnitudeMedia, "MEDIA 5X5 + MAGNITUDE");

		ImagemDigital.plotarImagem(magnitudeLimiar, "MAGNITUDE + LIMIAR");
		ImagemDigital.plotarImagem(magnitudeMediaLimiar, "MEDIA 5X5 + MAGNITUDE + LIMIAR");
	}

	public void questaoDez() {
		int[][] imagem = ImagemDigital.carregarImagem("Fig1038(a)(noisy_fingerprint).png");
		ImagemDigital.plotarImagem(limiar(imagem, 125), "LIMIAR");
		ImagemDigital.plotarImagem(limiar(aplicaMedia(imagem, 3, 3), 125), "LIMIAR + MEDIA");
		int limiar = limiarGlobalSimples(imagem, 200, 168);
		ImagemDigital.plotarImagem(limiar(imagem, limiar), "LIMIAR GLOBAL SIMPLES");
	}

	public void questaoOnze() {
		int[][] imagem = ImagemDigital.carregarImagem("Fig1039(a)(polymersomes).png");
		int limiar = limiarDeOtsu(imagem);
		ImagemDigital.plotarImagem(limiar(imagem, limiarGlobalSimples(imagem, media(imagem), 10)),
				"LIMIAR GLOBAL SIMPLES");
		System.out.println(limiar);
		ImagemDigital.plotarImagem(limiar(imagem, limiar), "OTSU");
		System.out.println(
				"O limiar globla simples encontra um ponto mediano entre as diferentes tonalidades de forma o objedecer um treshold, dependendo desse valor o algoritmo pode retornar diferentes pontos de limiarização. O algoritmo de Otsu por sua vez utiliza um valor de intensidade que maximize a variância entre as intensidades até ali calculada, retornando a intensidade de limiarização");
	}

	public void questaoDoze() {
		int[][] imagem = ImagemDigital.carregarImagem("Fig1039(a)(polymersomes).png");
		List<int[][]> list = divideImagem(imagem, 3, 3);
		for (int[][] im : list) {
			ImagemDigital.plotarImagem(im, "Original " + list.indexOf(im));
			ImagemDigital.plotarImagem(limiar(im, limiarDeOtsu(im)), "Imagem " + list.indexOf(im));
		}
	}

	public void questaoTreze() {
		int[][] imagem = ImagemDigital.carregarImagem("Fig1036(c)(gaussian_noise_mean_0_std_50_added).png");
		ImagemDigital.plotarImagem(limiar(imagem, limiarDeOtsu(imagem)), "Limiar de OTSU");
		int[][] media = aplicaMedia(imagem, 5, 5);
		ImagemDigital.plotarImagem(limiar(media, limiarDeOtsu(media)), "Limiar de OTSU com média 5 x 5");
	}

	public void questaoQuatorze() {
		int[][] imagem = ImagemDigital.carregarImagem("equacoes.png");
		ImagemDigital.plotarImagem(imagem, "original");

		int[][] semFundo = retiraFundo(imagem);
		ImagemDigital.plotarImagem(semFundo, "SEM FUNDO");

		int[][] limiarizada = limiar(semFundo, limiarDeOtsu(semFundo));
		ImagemDigital.plotarImagem(limiarizada, "LIMIAR DE OTSU");

	}

	private int[][] retiraFundo(int[][] imagem) {
		int retorno[][] = new int[imagem.length][imagem[0].length];
		for (int i = 0; i < imagem.length; i++) {
			for (int j = 0; j < imagem[0].length; j++) {
				if (imagem[i][j] == 0) {
					retorno[i][j] = 255;
				} else {
					retorno[i][j] = imagem[i][j];
				}
			}
		}
		return retorno;
	}

	private List<int[][]> divideImagem(int[][] imagem, int x, int y) {
		List<int[][]> list = new ArrayList<>();

		for (int lx = 0; lx < imagem.length; lx += imagem.length / x - 1) {
			for (int ly = 0; ly < imagem[0].length; ly += imagem.length / y - 1) {

				int[][] nova = new int[imagem.length / x][imagem[0].length / y];

				int i = 0;
				int j = 0;

				for (i = 0; i < nova.length; i++) {
					for (j = 0; j < nova[0].length; j++) {
						try {

							nova[i][j] = imagem[lx + i][ly + j];
						} catch (Exception ex) {

						}
					}
				}

				list.add(nova);
			}
		}

		return list;
	}

	private int media(int[][] imagem) {
		int m = 0;
		for (int i = 0; i < imagem.length; i++) {
			m += media(linha(imagem, i));
		}
		return m / imagem.length;
	}

	private int limiarGlobalSimples(int[][] imagem, int limiarInicial, int dT) {
		List<Integer> g1 = new ArrayList<>();
		List<Integer> g2 = new ArrayList<>();

		for (int i = 0; i < imagem.length; i++) {
			for (int j = 0; j < imagem[0].length; j++) {
				int valor = imagem[i][j];
				if (valor <= limiarInicial) {
					g1.add(valor);
				} else {
					g2.add(valor);
				}
			}
		}
		int mg1 = media(g1);
		int mg2 = media(g2);
		System.out.println("GRUPO 1: " + mg1);
		System.out.println("GRUPO 1: " + mg2);

		int media = (mg1 + mg2) / 2;
		System.out.println(media);

		if (Math.abs(limiarInicial - media) <= dT) {
			return media;
		} else {
			return limiarGlobalSimples(imagem, media, dT);
		}
	}

	private int limiarDeOtsu(int[][] original) {

		int[] histogram = calculaHistograma(original);
		int tamanhoImagem = original.length * original[0].length;

		float somaIntensidades = 0;
		for (int i = 0; i < 256; i++) {
			somaIntensidades += i * histogram[i];
		}

		float somaIntensidadesAtual = 0;
		int quantidadeIntensidadesAtual = 0;
		int intensidadesRestantes = 0;

		float varianciaMaxima = 0;
		int limiar = 0;

		for (int i = 0; i < 256; i++) {
			quantidadeIntensidadesAtual += histogram[i];

			// não há itensidade
			if (quantidadeIntensidadesAtual == 0)
				continue;
			intensidadesRestantes = tamanhoImagem - quantidadeIntensidadesAtual;

			// fim das intensidades
			if (intensidadesRestantes == 0)
				break;

			somaIntensidadesAtual += (float) (i * histogram[i]);

			// calcula a média das intensidades até agora
			float intensidadeMediaAtual = somaIntensidadesAtual / quantidadeIntensidadesAtual;

			// calcula a média das intensidades restantes
			float intensidadeMediaRestante = (somaIntensidades - somaIntensidadesAtual) / intensidadesRestantes;

			// calcula a variância média atual
			float varianciaAtual = (float) quantidadeIntensidadesAtual * (float) intensidadesRestantes
					* (float) Math.pow((intensidadeMediaAtual - intensidadeMediaRestante), 2);

			if (varianciaAtual > varianciaMaxima) {
				varianciaMaxima = varianciaAtual;
				limiar = i;
			}
		}

		return limiar;

	}

	private int[] calculaHistograma(int[][] imagem) {
		int[] histograma = new int[256];
		for (int i = 0; i < imagem.length; i++) {
			for (int j = 0; j < imagem[0].length; j++) {
				histograma[imagem[i][j]]++;
			}
		}
		return histograma;
	}

	private int media(List<Integer> list) {
		int i = 0;
		for (Integer v : list) {
			i += v;
		}
		try {
			return i / list.size();
		} catch (Exception ex) {
			return i;
		}
	}

	public static void writeToCSV(List<Integer> list, String nome) {
		try {
			BufferedWriter bw = new BufferedWriter(
					new OutputStreamWriter(new FileOutputStream(nome + ".csv"), "UTF-8"));
			for (Integer inteiro : list) {
				bw.write(String.valueOf(inteiro));
				bw.newLine();
			}
			bw.flush();
			bw.close();
		} catch (UnsupportedEncodingException e) {
		} catch (FileNotFoundException e) {
		} catch (IOException e) {
		}
	}

	private List<Integer> linha(int[][] imagem, int linha) {
		List<Integer> list = new ArrayList<>();
		int[] l = imagem[linha];
		for (int i = 0; i < l.length; i++) {
			list.add(l[i]);
		}
		return list;
	}

	private int[][] magnitude(int[][] imagem) {
		int[][] retorno = new int[imagem[0].length][imagem.length];
		for (int i = 0; i < retorno.length; i++) {
			for (int j = 0; j < retorno[0].length; j++) {
				try {
					int x = imagem[i + 1][j] - imagem[i][j];
					int y = imagem[i][j + 1] - imagem[i][j];
					retorno[i][j] = Math.abs(x) + Math.abs(y);
				} catch (Exception ex) {
					retorno[i][j] = 0;
				}
			}
		}
		return retorno;
	}

	public int[][] soma(int[][] um, int[][] dois, int k) {
		int[][] retorno = new int[um.length][um[0].length];

		for (int i = 0; i < um.length; i++) {
			for (int j = 0; j < um[0].length; j++) {
				int valor = um[i][j] + (k * dois[i][j]);
				retorno[i][j] = valor;
			}
		}

		return normaliza(retorno);
	}

	public int[][] normaliza(int[][] imagem) {
		int[] min_max = min_max(imagem);
		int[][] retorno = new int[imagem.length][imagem[0].length];

		for (int i = 0; i < imagem.length; i++) {
			for (int j = 0; j < imagem[0].length; j++) {
				retorno[i][j] = 255 * (imagem[i][j] - min_max[0]) / (min_max[1] - min_max[0]);
			}
		}
		return retorno;
	}

	private int[] min_max(int[][] imagem) {
		int min = 255;
		int max = 0;
		for (int i = 0; i < imagem.length; i++) {
			for (int j = 0; j < imagem[0].length; j++) {
				if (imagem[i][j] > max) {
					max = imagem[i][j];
				}
				if (imagem[i][j] < min) {
					min = imagem[i][j];
				}
			}
		}
		return new int[] { min, max };
	}

	public int[][] subtrai(int[][] um, int[][] dois) {
		int[][] retorno = new int[um.length][um[0].length];

		for (int i = 0; i < um.length; i++) {
			for (int j = 0; j < um[0].length; j++) {
				retorno[i][j] = um[i][j] - dois[i][j];
			}
		}

		return retorno;
	}

	public int[][] aplicaFiltro(int[][] imagem, int[][] filtro) {
		int[][] retorno = new int[imagem.length][imagem[0].length];
		for (int i = 0; i < imagem.length; i++) {
			for (int j = 0; j < imagem[0].length; j++) {
				retorno[i][j] = imagem[i][j];
			}
		}
		for (int i = 0; i < imagem.length; i++) {
			for (int j = 0; j < imagem[0].length; j++) {

				try {
					int soma = 0;

					for (int k = 0; k < filtro.length; k++) {
						for (int l = 0; l < filtro[0].length; l++) {
							soma += retorno[i + k][j + l] * filtro[k][l];
						}
					}

					retorno[i][j] = soma > 255 ? 255 : soma < 0 ? 0 : soma;
				} catch (Exception ex) {

				}

			}
		}
		return retorno;
	}

	public int[][] aplicarLaplaciano(int[][] imagem, int[][] filtro) {

		int[][] retorno = new int[imagem.length][imagem[0].length];
		// percorre cada pixel
		for (int i = filtro.length / 2; i < imagem.length - filtro.length / 2; i++) {
			for (int j = filtro[0].length / 2; j < imagem[0].length - filtro[0].length / 2; j++) {

				// percorre a janela
				for (int w = i - (filtro.length / 2); w <= i + (filtro.length / 2); w++) {
					for (int h = j - (filtro[0].length / 2); h <= j + (filtro[0].length / 2); h++) {

						// percorrendo o filtro
						for (int k = 0; k < filtro.length; k++) {
							for (int l = 0; l < filtro[0].length; l++) {

								if (w + k < imagem.length && h + l < imagem[0].length) {
									retorno[w + k][h + l] = imagem[w + k][h + l] + imagem[w + k][h + l]
											+ imagem[w + k][h + l] + imagem[w + k][h + l] - 4 * imagem[w + k][h + l];

								}

							}
						}

					}
				}

			}
		}

		return retorno;
	}

	private int[][] ressaltaHorizontal(int[][] imagem) {
		int retorno[][] = new int[imagem.length][imagem[0].length];
		for (int i = 0; i < imagem.length; i++) {
			for (int j = 0; j < imagem[0].length; j++) {
				try {
					retorno[i][j] = Math.abs(imagem[i][j + 1] - imagem[i][j]);
				} catch (Exception ex) {
					retorno[i][j] = 0;
				}
			}
		}
		return retorno;
	}

	private int[][] ressaltaVertical(int[][] imagem) {
		int retorno[][] = new int[imagem.length][imagem[0].length];
		for (int i = 0; i < imagem.length; i++) {
			for (int j = 0; j < imagem[0].length; j++) {
				try {
					retorno[i][j] = Math.abs(imagem[i + 1][j] - imagem[i][j]);
				} catch (Exception ex) {
					retorno[i][j] = 0;
				}
			}
		}
		return retorno;
	}

	private int[][] ressaltaAngulo(int[][] imagem, double angulo) {
		int retorno[][] = new int[imagem.length][imagem[0].length];
		for (int i = 0; i < imagem.length; i++) {
			for (int j = 0; j < imagem[0].length; j++) {
				try {

					int x = imagem[i + 1][j] - imagem[i][j];
					int y = imagem[i][j + 1] - imagem[i][j];

					int v = (int) (Math.atan(y / x) * 180 / Math.PI);

					retorno[i][j] = v == angulo ? 255 : 0;

				} catch (Exception ex) {
					retorno[i][j] = 0;
				}
			}
		}
		return retorno;
	}

	private int[][] aplicaMediana(int[][] imagem, int janela_w, int janela_h) {
		int[][] retorno = new int[imagem.length][imagem[0].length];

		// percorre cada pixel
		for (int i = janela_w / 2; i < imagem.length - janela_w / 2; i++) {
			for (int j = janela_h / 2; j < imagem[0].length - janela_h / 2; j++) {
				List<Integer> list = new ArrayList<Integer>();
				// percorre a janela
				for (int w = i - (janela_w / 2); w <= i + (janela_w / 2); w++) {
					for (int h = j - (janela_h / 2); h <= j + (janela_h / 2); h++) {
						// para cada valor do pixel incrementa no map
						// Integer last = map.get(imagem[w][h]);
						// map.put(imagem[w][h], (last != null ? last : 0) + 1);
						list.add(imagem[w][h]);
					}
				}
				// int maior = 0;
				// // verifica qual valor de maior ocorrencia
				// for (Integer integer : map.keySet()) {
				// if (maior < map.get(integer)) {
				// maior = integer;
				// }
				// }
				// retorno[i][j] = maior;
				Integer[] a = (Integer[]) list.toArray(new Integer[list.size()]);
				Arrays.sort(a);
				retorno[i][j] = a[a.length / 2];
			}
		}

		return retorno;
	}

	private int[][] limiar(int[][] imagem, int limiar) {
		int[][] retorno = new int[imagem.length][imagem[0].length];

		for (int i = 0; i < imagem.length; i++) {
			for (int j = 0; j < imagem[0].length; j++) {
				int p = imagem[i][j];
				if (p <= limiar) {
					retorno[i][j] = 0;
				} else {
					retorno[i][j] = 255;

				}

			}
		}

		return retorno;
	}

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Lista3 l3 = new Lista3();
		// l3.questaoUm();
		// l3.questaoDois();
		// l3.questaoTres();
		// l3.questaoQuatro();
		// l3.questaoCinco();
		// l3.questaoSeis();
		// l3.questaoSete();
		// l3.questaoOito();
		// l3.questaoNove();
		// l3.questaoDez();
		// l3.questaoOnze();
		// l3.questaoDoze();
		// l3.questaoTreze();
		// l3.questaoQuatorze();
		// l3.questaoOitoListaDois();

	}

}
